# sqlitewtview
An example project that uses vala-gtk+, sqlite, and treeview/listbox synchronizing data between Gtk liststore and database. I could not find so many \
tutorials about handling liststore, treeview events, listbox and data manipulation with vala, so I decided to make an example project. Hope someone \
find it useful.

## Features
* this project shows an example usage of gtk+ treeview/listbox and sqlite using vala synchronizing data between Gtk+ treeview/listbox, liststore, and database.

## Requirements

* meson >= 0.40.0
* gio-2.0 >= 2.48.2
* gtk+-3.0 >= 3.20
* Sqlite3 >= 3.21
* gee-0.8 >= 0.20
  
## How to build
clone the project and:
```
cd sqlitewtview
mkdir build
cd build
meson ..
ninja
```
Precompiled binary for Windows 64bit users can be found in tag section.
I am pretty sure that it can also be compiled on Posix systems if dependencies are met.

## Preview:
* ![alt text](./win_res/sshot.png?raw=true)
* ![alt text](./win_res/sshot2.png?raw=true)
