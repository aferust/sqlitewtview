/* window.vala
 *
 * Copyright 2018 user
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
using GLib;
using Gee;
using Sqlite;

namespace Sqlitewtview {
	[GtkTemplate (ui = "/org/gnome/Sqlitewtview/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		
		[GtkChild]
		Gtk.Box box1;
		
		[GtkChild]
		Gtk.Button but1;
		
		[GtkChild]
		Gtk.Button but2;		
		
		[GtkChild]
		Gtk.Button but3;
		
		[GtkChild]
		Gtk.RadioButton opt_by_email;
		
		[GtkChild]
		Gtk.RadioButton opt_by_name;
		
		[GtkChild]
		Gtk.Entry entry1;
		
		Sqlite.Database db;
		string errmsg;
		Gtk.ListStore list_store;
		Gtk.TreeIter iter;
		delegate void DelegateType ();
		
		public Window (Gtk.Application app) {
			Object (application: app);
			
			this.border_width = 5;
			
			opt_by_name.active = true;
			//this.icon = new Gdk.Pixbuf.from_resource("/org/gnome/Sqlitewtview/../win_res/app.ico");
			
			list_store = new Gtk.ListStore (6, typeof(int), typeof(string), typeof(string), typeof(string), typeof(string), typeof (bool));
			
			Gtk.TreeView view = new Gtk.TreeView.with_model (list_store);
			box1.add(view);
			
			Gtk.TreeSelection sel = view.get_selection();
			
			update_list_store_from_db();
			
			CellRendererText cell_id = new CellRendererText ();
			cell_id.editable = false;
			
			CellRendererText cell = new CellRendererText ();
			cell.editable = true;
			
			CellRendererToggle cell_toggle = new CellRendererToggle();
			
			cell_toggle.toggled.connect ((toggle, path) => {
				Gtk.TreePath tree_path = new Gtk.TreePath.from_string (path);
				Gtk.TreeIter itr;
				list_store.get_iter (out itr, tree_path);
				list_store.set (itr, 5, !cell_toggle.active);
			});
			
			TreeViewColumn id_col = new TreeViewColumn.with_attributes ("id", cell_id, "text", 0);
			TreeViewColumn email_col = new TreeViewColumn.with_attributes ("E-mail", cell, "text", 1);
			TreeViewColumn name_col = new TreeViewColumn.with_attributes ("Name", cell, "text", 2);
			TreeViewColumn age_col = new TreeViewColumn.with_attributes ("Age", cell, "text", 3);
			TreeViewColumn gender_col = new TreeViewColumn.with_attributes ("Gender", cell, "text", 4);
			Gtk.TreeViewColumn tog_col = new Gtk.TreeViewColumn ();
			tog_col.title = "Selection";
			tog_col.pack_start (cell_toggle, false);
			tog_col.add_attribute (cell_toggle, "active", 5);
			
			id_col.set_sort_column_id(0);
			email_col.set_sort_column_id(1);
			name_col.set_sort_column_id(2);
			age_col.set_sort_column_id(3);
			gender_col.set_sort_column_id(4);
			
			view.insert_column(id_col, -1);
			view.insert_column(email_col, -1);
			view.insert_column(name_col, -1);
			view.insert_column(age_col, -1);
			view.insert_column(gender_col, -1);
			view.insert_column(tog_col, -1);
			
			
			
			but1.clicked.connect (() => {
				int ec = Sqlite.Database.open ("test.db", out db);
				if (ec != Sqlite.OK) {
					stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				}
				string query = """
					INSERT INTO User (email, name, age, gender) VALUES ('empty', 'empty', 0, 0);
					""";
				ec = db.exec (query, null, out errmsg);
				if (ec != Sqlite.OK) {
					stderr.printf ("Error: %s\n", errmsg);
				}else{
					int64 last_id = db.last_insert_rowid ();
					print ("Last inserted id: %" + int64.FORMAT + "\n", last_id);
					
					list_store.append (out iter);
					list_store.set (iter, 0, last_id, 1, "empty", 2, "empty", 3, "0", 4, "0");
					remove_dummy_if_any_record_exist();
				}


			});
			
			but2.clicked.connect(()=>{
				update_list_store_from_db();
			});
			
			entry1.changed.connect(()=>{
				string search_str = entry1.get_text();
				bool em_active = opt_by_email.get_active();
				update_list_store_on_search(search_str, em_active?"email":"name");
			});
			
			int cur_id = -1;
			sel.changed.connect((selection)=>{
				Gtk.TreeModel model;
				Gtk.TreeIter it;
				
				bool _is_set = selection.get_selected(out model, out it);
				iter = it;
				if(_is_set){
					GLib.Value col0;
					model.get_value(it, 0, out col0);
					
					cur_id = col0.get_int();
					
					stdout.printf("selected: %d\n", cur_id);
				}

			});
			
			DelegateType fireRightClckCtx = ()=>{ // this is a lambda function. Why? Because we can :)
				Gtk.Menu menu = new Gtk.Menu ();
				Gtk.MenuItem menu_item = new Gtk.MenuItem.with_label("delete this");
				
					menu_item.activate.connect(()=>{
					Gtk.TreeModel model;
					Gtk.TreeIter it;
					if(sel.get_selected(out model, out it)){
						int nrows = model.iter_n_children(null);
						if(nrows != 1 ){
							delete_row(cur_id);
							list_store.remove(ref it);				
						}else{
							delete_row(cur_id);
							list_store.remove(ref it);
							add_dummy_row();
						}

					}
				});
				menu.attach_to_widget (view, null);
				menu.add (menu_item);
				menu.show_all ();
				
				menu.popup_at_pointer();
			};
			
			view.key_press_event.connect ((event) => {
				if(event.keyval == 65383){// keyboard's right click key
					fireRightClckCtx();
				}
				return false;
			});
			
			view.button_press_event.connect ((event) => {
				if (event.type == Gdk.EventType.BUTTON_PRESS && event.button == 3) {// mouse's right click 
					fireRightClckCtx();
				}
				return false;
			});
			
			/* editing the cells */
			TreePath? path_to_edit = null;
			TreeViewColumn? column_to_edit = null;
			
			view.cursor_changed.connect(()=>{
				view.get_cursor (out path_to_edit, out column_to_edit);
			});
			
			cell.edited.connect((path, text)=>{
				int cid = column_to_edit.sort_column_id;
				
				Gtk.TreeModel model;
				GLib.Value valId;
				sel.get_selected(out model, out iter);
				list_store.get_value (iter, 0, out valId);
				if((int)valId == -1) return; // do not modify dummy row.
				
				switch (cid) {
				/* case 0 is skipped.*/
				case 1:
					list_store.set_value(iter, cid, text);
					break;
				case 2:
					list_store.set_value(iter, cid, text);
					break;
				case 3:
					list_store.set_value(iter, cid, int.parse(text));
					break;
				case 4:
					list_store.set_value(iter, cid, int.parse(text));
					break;
				}
				
				update_cell_on_db(cid, cur_id.to_string(), text.to_string());
			});
			
			/* editing the cells end */
			
			/* deleting selected rows */
			but3.clicked.connect(()=>{ // delete selected rows
				var to_be_deleted = new Gee.HashMap<int, Gtk.TreeIter?>();
				
				// a good way of looping over liststore with list_store.foreach ()
				// in this way we can also access TreeModel obj for getting num of rows if needed.
				// there is another way of looping in https://valadoc.org/gtk+-3.0/Gtk.TreeIter.html
				Gtk.TreeModel current_model = null;
				
				Gtk.TreeModelForeachFunc determine_del_rows = (model, path, iter) => { 
					GLib.Value valSelected, valId;
					list_store.get_value (iter, 5, out valSelected);
					list_store.get_value (iter, 0, out valId);
					
					bool is_selected = (bool)valSelected;
					if(is_selected){
						int id = (int)valId;
						to_be_deleted.set(id, iter);
					}
					current_model = model;
					return false;
				};
				list_store.foreach (determine_del_rows);
				
				foreach (var entry in to_be_deleted.entries) {
					int id = entry.key;
					Gtk.TreeIter it = entry.value;
					int nrows = current_model.iter_n_children(null);
					if(nrows != 1 ){
						delete_row(id);
						list_store.remove(ref it);				
					}else{
						delete_row(id);
						list_store.remove(ref it);
						add_dummy_row();
					}
				}
				
			});
			/* deleting selected rows end*/
			
			this.show_all ();
		}
		
		public void add_dummy_row(){
			list_store.append (out iter);
			list_store.set (iter, 0, -1, 1, "no record", 2, "no record", 3, "no record", 4, "no record");
		}
		
		public void remove_dummy_if_any_record_exist(){
			Gtk.TreeModelForeachFunc parse_rows = (model, path, iter) => { 
				GLib.Value valId;
				list_store.get_value (iter, 0, out valId);
				int id = (int)valId;
				if(id == -1) list_store.remove(ref iter);
				return false;
			};
			list_store.foreach (parse_rows);
		}
		
		public int update_list_store_from_db(){
			list_store.clear();
			
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			string query = "SELECT * FROM User";
			int count = 0;
			ec = db.exec (query, (n_columns, values, column_names)=> { /*int n_columns, string[] values, string[] column_names*/
				list_store.append (out iter);
				list_store.set (iter, 0, int.parse(values[0]), 1, values[1], 2, values[2], 3, values[3], 4, values[4]);
				count ++;
				return 0;
			}, out errmsg);
			
			if(count == 0) add_dummy_row();
			
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}
			return 0;			
		}
		
		public int update_list_store_on_search(string search_str, string field_name){
			list_store.clear();
			
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			Sqlite.Statement stmt;
			string prepared_query_str = @"SELECT * FROM User WHERE $field_name LIKE ?";
			//in above sql string ? will be replaced by bind_text() safely
			// for field name we have to use valas string templates like $field_name
			
			ec = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out stmt);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			// to avoid sql injection we do not simply concat strings
			// However, we can concat "%" char safely.
			// bind_text escapes all dangerous inputs.
			
			string search_str_final = "%" + search_str + "%";
			stmt.bind_text (1, search_str_final);
			
			int count = 0;
			while (stmt.step () == Sqlite.ROW) {
				int id = stmt.column_int(0);
				string email = stmt.column_text(1);
				string name = stmt.column_text(2);
				int age = stmt.column_int(3);
				int gender = stmt.column_int(4);
				
				list_store.append (out iter);
				list_store.set (iter, 0, id, 1, email, 2, name, 3, age.to_string(), 4, gender.to_string());
				
				count ++;
			}
			/* There is an issue that an empty liststore causes crashes on windows: 
			 * https://gitlab.gnome.org/GNOME/gtk/issues/1333
			 * https://gitlab.freedesktop.org/cairo/cairo/issues/338
			 * So we simply put an dummy row to avoid crashes.
			*/
			if(count == 0){
				add_dummy_row();
			}
			return 0;			
		}
		
		public int delete_row(int id){
			string errmsg;

			// Open a database:
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}

			string query = "DELETE FROM User WHERE id = %d;".printf(id);
			ec = db.exec (query, null, out errmsg);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}

			int changes = db.total_changes ();
			print ("Affected rows: %d\n", changes);

			return 0;
		}
		
		public int update_cell_on_db(int cid, string id, string val){
			// I am also a python fan, so I love dictionaries and hashmaps a lot!
			var fields = new HashMap<int, string> ();
			fields.set (1, "email");
			fields.set (2, "name");
			fields.set (3, "age");
			fields.set (4, "gender");
			
			string field = fields.get(cid);
			
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			Sqlite.Statement stmt;
			string prepared_query_str = @"UPDATE User SET $field = ? WHERE id = ?;";
			
			ec = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out stmt);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			stmt.bind_text (1, val);// why does wildcard index start from 1? why sqlite why?
			stmt.bind_text (2, id);
			
			stmt.step(); // I think it does something like exec here? Anyway it works. 
			
			int changes = db.total_changes ();
			print ("Affected rows: %d\n", changes);

			return 0;
		}
	}
}
