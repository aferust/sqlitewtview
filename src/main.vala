/* main.vala
 *
 * Copyright 2018 user
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main (string[] args) {
	// only for windows deployment
	/* 
	if(GLib.Environment.get_variable("GSETTINGS_SCHEMA_DIR")==null){
		string sep = GLib.Path.DIR_SEPARATOR_S;
		GLib.Environment.set_variable ("GSETTINGS_SCHEMA_DIR", GLib.Environment.get_current_dir() + @"$(sep)share$(sep)glib-2.0", true);
		stdout.printf("%s \n", GLib.Environment.get_variable("GSETTINGS_SCHEMA_DIR"));
	}
	*/
	// only for windows deployment end
	
	var app = new Gtk.Application ("org.gnome.Sqlitewtview", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new Sqlitewtview.Welcome (app);
		}
		win.present ();
	});
	
	return app.run (args);
}
