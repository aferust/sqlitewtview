namespace Sqlitewtview {
	[GtkTemplate (ui = "/org/gnome/Sqlitewtview/welcome.ui")]
	public class Welcome : Gtk.ApplicationWindow {
		[GtkChild]
		Gtk.Button but1;
		
		[GtkChild]
		Gtk.Button but2;
		
		Sqlite.Database db;
		string errmsg;
		
		public Welcome (Gtk.Application app) {
			Object (application: app);
			
			createMyDB(); // create db if not exist
			
			but1.clicked.connect(()=>{
				var win = new Window(app);
				win.present();
			});
			
			but2.clicked.connect(()=>{
				var win = new Window2(app);
				win.present();
			});
		}
		
		public int createMyDB(){

			// Open/Create a database:
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}


			string query = """
				CREATE TABLE User (
					id		INTEGER PRIMARY KEY AUTOINCREMENT,
					email	TEXT	NOT NULL,
					name	TEXT	NOT NULL,
					age		INT		NOT NULL,
					gender	INT		NOT NULL
				);
				""";

			// Execute our query:
			ec = db.exec (query, null, out errmsg);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}
			insertSomeData();
			print ("Created.\n");

			return 0;
		}
		
		public int insertSomeData(){
			// Open a database:
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			string query = """
				INSERT INTO User ( email, name, age, gender) VALUES ('foo1@bar.com', 'foo barson', 20, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo2@bar.com', 'Sheryl crow', 30, 1);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo3@bar.com', 'Dan Patlansky', 30, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo4@bar.com', 'Asım Can Gündüz', 20, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo5@bar.com', 'Chris Rea', 20, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo6@bar.com', 'Stevei Ray Vaughan', 20, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo7@bar.com', 'Ayhan Sicimoğlu', 20, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo7@bar.com', 'Doğan Canku', 20, 0);
				INSERT INTO User ( email, name, age, gender) VALUES ('foo7@bar.com', 'İlter Kurcala', 20, 0);
				""";
			ec = db.exec (query, null, out errmsg);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}

			int64 last_id = db.last_insert_rowid ();
			print ("Last inserted id: %" + int64.FORMAT + "\n", last_id);

			return 0;
		}
	}
}
