/* window.vala
 *
 * Copyright 2018 user
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
using GLib;
using Gee;
using Sqlite;

namespace Sqlitewtview {
	
	public class ListBoxRowWithData: Gtk.ListBoxRow{
		// we are not using liststore or any data model here
		// we simply extend ListBoxRow class for handling rows easily
		public int id;
		public int age;
		public int gender;
		public string email;
		public string uname;
		public Window2 ctx;
		
		public Gtk.Label label_id;
		public Gtk.Entry entry_email;
		public Gtk.Entry entry_name;
		public Gtk.SpinButton entry_age;
		public Gtk.ComboBoxText entry_gender;
		public Gtk.Button but_del;
		public Gtk.Button expand_but;
		
		public bool eXpanded = false;
		
		public static int id_width;
		public static int em_width;
		public static int nm_width;
		public static int ag_width;
		public static int gn_width;
		public static int dl_width;
		public static int ex_width;
		
		public ListBoxRowWithData (int _id, string _email, string _name, int _age, int _gender, Window2 _ctx) {
			id = _id; email = _email; uname = _name; age = _age; gender = _gender; ctx = _ctx;
			
			Gtk.Box expand_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
			var expand_label = new Gtk.Label("Additional details of id:%d can place here!".printf(id));
			Gtk.Frame frame = new Gtk.Frame ("Details here");
			frame.add(expand_label);
			expand_box.pack_start (frame, true, true, 0);
			expand_but = new Gtk.Button.from_icon_name("go-down");
			
			label_id = new Gtk.Label("%d".printf(id));
			entry_email = new Gtk.Entry(); entry_email.set_text(email);
			entry_name = new Gtk.Entry(); entry_name.set_text(uname);
			entry_age = new Gtk.SpinButton.with_range (0, 150, 1); entry_age.set_value(age);
			entry_gender = new Gtk.ComboBoxText();
			
			entry_gender.append_text ("Male");
			entry_gender.append_text ("Female");
			entry_gender.append_text ("Not Specified");
			entry_gender.active = gender;
			
			entry_email.changed.connect(()=>{
				ctx.update_field(id, "email", entry_email.get_text());
			});
			
			entry_name.changed.connect(()=>{
				ctx.update_field(id, "name", entry_name.get_text());
			});
			
			entry_age.changed.connect(()=>{
				ctx.update_field(id, "age", entry_age.get_value_as_int().to_string());
			});
			
			entry_gender.changed.connect(()=>{
				ctx.update_field(id, "gender", entry_gender.active.to_string());
			});
			
			but_del = new Gtk.Button.with_label("delete");
			but_del.clicked.connect(()=>{
				//ctx.listbox.unselect_row(this as Gtk.ListBoxRow);
				if(ctx.delete_row(id)== 0){
					//ctx.populate_list();
					ctx.clear_listbox(id);
				}
			});
			
			Gtk.Box rbox = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
			Gtk.Box inner_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
			
			inner_box.pack_start (label_id, true, true, 0);
			inner_box.pack_start (entry_email, true, true, 0);
			inner_box.pack_start (entry_name, true, true, 0);
			inner_box.pack_start (entry_age, true, true, 0);
			inner_box.pack_start (entry_gender, true, true, 0);
			inner_box.pack_start (but_del, true, true, 0);
			inner_box.pack_start (expand_but, false, false, 0);
			
			rbox.pack_start (inner_box, true, true, 0);
			
			expand_but.clicked.connect(()=>{
				eXpanded = !eXpanded;
				if(eXpanded == true){
					rbox.add (expand_box);
					expand_but.set_image(new Image.from_icon_name("go-up", IconSize.BUTTON));
					rbox.show_all();
				}else{
					rbox.remove (expand_box);
					expand_but.set_image(new Image.from_icon_name("go-down", IconSize.BUTTON));
				}
			});
			
			this.add(rbox); 
			
		}
		
		public void init_widths(){
			if(id_width == 0){
				id_width = label_id.get_allocated_width();
				em_width = entry_email.get_allocated_width();
				nm_width = entry_name.get_allocated_width();
				ag_width = entry_age.get_allocated_width();
				gn_width = entry_gender.get_allocated_width();
				dl_width = expand_but.get_allocated_width();
				ex_width = expand_but.get_allocated_width();
			}
		}
	}
	
	[GtkTemplate (ui = "/org/gnome/Sqlitewtview/window2.ui")]
	public class Window2 : Gtk.ApplicationWindow {
		
		[GtkChild]
		public Gtk.ListBox listbox;
		
		[GtkChild]
		Gtk.Button but1;
		
		[GtkChild]
		Gtk.RadioButton opt_by_name;
		
		[GtkChild]
		Gtk.RadioButton opt_by_email;
		
		[GtkChild]
		Gtk.SearchEntry s_entry;
		
		[GtkChild]
		Gtk.Switch sw1;
		
		Sqlite.Database db;
		string errmsg;
		
		Gtk.Box fake_header;
		
		public Window2 (Gtk.Application app) {
			Object (application: app);
			
			listbox.set_selection_mode(SelectionMode.NONE);
			
			opt_by_name.active = true;
			
			populate_list();
			align_my_header();
			
			but1.clicked.connect(()=>{
				int ec = Sqlite.Database.open ("test.db", out db);
				if (ec != Sqlite.OK) {
					stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				}
				string query = """
					INSERT INTO User (email, name, age, gender) VALUES ('empty', 'empty', 0, 0);
					""";
				ec = db.exec (query, null, out errmsg);
				if (ec != Sqlite.OK) {
					stderr.printf ("Error: %s\n", errmsg);
				}else{
					int64 last_id = db.last_insert_rowid ();
					print ("Last inserted id: %" + int64.FORMAT + "\n", last_id);
					
					listbox.add (new ListBoxRowWithData((int)last_id, "empty", "empty", 0, 0, this));
					
					listbox.show_all();
				}
			});
			
			sw1.notify["active"].connect(()=>{
				if(sw1.get_active()){
					listbox.set_sort_func((_row1, _row2)=>{
						var row1 = _row1 as ListBoxRowWithData;
						var row2 = _row2 as ListBoxRowWithData;
						
						if(row1.id < row2.id) return 1;
						
						return 0;
					});
				}else{
					listbox.set_sort_func((_row1, _row2)=>{
						var row1 = _row1 as ListBoxRowWithData;
						var row2 = _row2 as ListBoxRowWithData;
						
						if(row1.id > row2.id) return 1;
						return 0;
					});
				}
				
			});

			/* we can enable this if we set SelectionMode other than NONE
			listbox.row_selected.connect(()=>{
				var row = listbox.get_selected_row() as ListBoxRowWithData;
				if(row != null) stdout.printf("%d \n", row.id);// as you see, we can access row id easily
			});
			*/
			/*
			listbox.@foreach ((_row) => {
				var row = _row as ListBoxRowWithData;
				stdout.printf("%d \n", row.id);
				
			});
			*/
			s_entry.changed.connect(()=>{
				string search_str = s_entry.get_text();
				bool em_active = opt_by_email.get_active();
				populate_on_search(search_str, em_active?"email":"name");
			});
			
			this.show_all ();
		}
		
		public void align_my_header(){
			// listbox has no built-in something like a table header
			// my solution is to set a row header to the first row,
			// by updating title sizes based on row contents.
			
			listbox.set_header_func((_row, _before)=>{
				var row = _row as ListBoxRowWithData;
				fake_header = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
				var id = new Gtk.Label("id");
				var em = new Gtk.Label("email");
				var nm = new Gtk.Label("name");
				var ag = new Gtk.Label("age");
				var gn = new Gtk.Label("gender");
				var dl = new Gtk.Label("delete");
				var ex = new Gtk.Label("expand");
				
				fake_header.pack_start (id, true, true, 0);
				fake_header.pack_start (em, true, true, 0);
				fake_header.pack_start (nm, true, true, 0);
				fake_header.pack_start (ag, true, true, 0);
				fake_header.pack_start (gn, true, true, 0);
				fake_header.pack_start (dl, true, true, 0);
				fake_header.pack_start (ex, false, false, 0);
				
				if(row.get_index()==0){

					fake_header.realize.connect(()=>{
						row.init_widths();
						
						id.set_size_request(ListBoxRowWithData.id_width, -1);
						em.set_size_request(ListBoxRowWithData.em_width, -1);
						nm.set_size_request(ListBoxRowWithData.nm_width, -1);
						ag.set_size_request(ListBoxRowWithData.ag_width, -1);
						gn.set_size_request(ListBoxRowWithData.gn_width, -1);
						dl.set_size_request(ListBoxRowWithData.dl_width, -1);
						ex.set_size_request(ListBoxRowWithData.ex_width, -1);
					});
					
					row.set_header(fake_header);
					fake_header.show_all();
					
				}else{
					row.set_header(null);
				}
				
			});
		}
		
		public void clear_listbox(int id = -1){
			var allkids = listbox.get_children();
			if(id == -1){
				allkids.foreach ((entry) => {
					listbox.remove(entry);
				});	
			}else{
				allkids.foreach ((entry) => {
					if((entry as ListBoxRowWithData).id == id) listbox.remove(entry);
				});	
			}
			
		}
		
		public int populate_list(){
			clear_listbox();
			
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			string query = "SELECT * FROM User";
			int count = 0;
			ec = db.exec (query, (n_columns, values, column_names)=> { /*int n_columns, string[] values, string[] column_names*/
				listbox.add (new ListBoxRowWithData(int.parse(values[0]), values[1], values[2], int.parse(values[3]), int.parse(values[4]), this));
				count ++;
				return 0;
			}, out errmsg);
			
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}
			
			listbox.show_all();
			
			return 0;
		}
		
		public int populate_on_search(string search_str, string field_name){
			clear_listbox();
			
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			Sqlite.Statement stmt;
			string prepared_query_str = @"SELECT * FROM User WHERE $field_name LIKE ?";
			
			ec = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out stmt);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			string search_str_final = "%" + search_str + "%";
			stmt.bind_text (1, search_str_final);
			
			int count = 0;
			while (stmt.step () == Sqlite.ROW) {
				int id = stmt.column_int(0);
				string email = stmt.column_text(1);
				string name = stmt.column_text(2);
				int age = stmt.column_int(3);
				int gender = stmt.column_int(4);
				
				var row = new ListBoxRowWithData(id, email, name, age, gender, this);
				listbox.add (row);
				
				count ++;
			}
			
			listbox.show_all();
			
			return 0;			
		}
		
		public int update_field(int id, string field, string val){
			
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			Sqlite.Statement stmt;
			string prepared_query_str = @"UPDATE User SET $field = ? WHERE id = ?;";
			
			ec = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out stmt);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}
			
			stmt.bind_text (1, val);// why does wildcard index start from 1? why sqlite why?
			stmt.bind_int (2, id);
			
			stmt.step(); // I think it does something like exec here? Anyway it works. 
			
			int changes = db.total_changes ();
			print ("Affected rows: %d\n", changes);

			return 0;
		}
		
		public int delete_row(int id){
			string errmsg;

			// Open a database:
			int ec = Sqlite.Database.open ("test.db", out db);
			if (ec != Sqlite.OK) {
				stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
				return -1;
			}

			string query = "DELETE FROM User WHERE id = %d;".printf(id);
			ec = db.exec (query, null, out errmsg);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}

			int changes = db.total_changes ();
			print ("Affected rows: %d\n", changes);

			return 0;
		}
	}
}
